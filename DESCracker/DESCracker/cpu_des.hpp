#include <bitset>

std::string HexCharToBin(char c);
void RunDesOnCpu();

std::string des_encrypt_block_cpu(std::string key, std::string message);
std::string des_decrypt_block_cpu(std::string key, std::string encrypted_message);