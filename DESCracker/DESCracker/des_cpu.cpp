#include "cpu_des.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>

using namespace std;

char permutation1[] = {
    57, 49, 41, 33, 25, 17, 9,
    1, 58, 50, 42, 34, 26, 18,
    10, 2, 59, 51, 43, 35, 27,
    19, 11, 3, 60, 52, 44, 36,
    63, 55, 47, 39, 31, 23, 15,
    7, 62, 54, 46, 38, 30, 22,
    14, 6, 61, 53, 45, 37, 29,
    21, 13, 5, 28, 20, 12, 4
};

char shifts1[] = {
    0, 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};

char permutation2[] = {
    14, 17, 11, 24, 1, 5,
    3, 28, 15, 6, 21, 10,
    23, 19, 12, 4, 26, 8,
    16, 7, 27, 20, 13, 2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32
};

char initial_permutation[] = {
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17, 9, 1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7
};

char e_bit_table[] = {
    32, 1, 2, 3, 4, 5,
    4, 5, 6, 7, 8, 9,
    8, 9, 10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32, 1
};

char s1[] = {
    14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
    0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
    4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
    15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
};

char s2[] = {
    15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
    3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
    0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
    13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
};

char s3[] = {
    10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
    13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
    13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
    1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
};

char s4[] = {
    7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
    13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
    10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
    3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
};

char s5[] = {
    2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
    14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
    4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
    11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
};

char s6[] = {
    12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
    10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
    9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
    4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
};

char s7[] = {
    4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
    13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
    1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
    6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
};

char s8[] = {
    13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
    1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
    7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
    2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
};

char *s[] = { s1, s2, s3, s4, s5, s6, s7, s8 };

char P[] = {
    16, 7, 20, 21,
    29, 12, 28, 17,
    1, 15, 23, 26,
    5, 18, 31, 10,
    2, 8, 24, 14,
    32, 27, 3, 9,
    19, 13, 30, 6,
    22, 11, 4, 25
};

char final_permutation[] = {
    40, 8, 48, 16, 56, 24, 64, 32,
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41, 9, 49, 17, 57, 25
};

uint64_t decrypt_gpu(uint64_t key, int64_t encrypted_message);

void RunDesOnCpu()
{
    string hex_key = "133457799BBCDFF1";
    string bin_key = HexStringToBinaryString(hex_key);

    string message = "Your lips are smoother than vaseline";
    string hex_message = "0123456789ABCDEF";
    string bin_message = HexStringToBinaryString(hex_message);

    for(int i = hex_message.length(); i % 16 != 0; i++)
    {
        hex_message.append("0");
    }

    auto encrypted_message = des_encrypt_block_cpu(bin_key, bin_message);

    cout << "plaintext message: " << hex_message << endl;
    cout << "encrypted message: " << encrypted_message << endl;

    auto bin_encrypted = HexStringToBinaryString(encrypted_message);
    auto decrypted_message = des_decrypt_block_cpu(bin_key, bin_encrypted);

    uint64_t plain_int = stoull(bin_message, nullptr, 2);

    uint64_t key_int = stoull(bin_key, nullptr, 2);
    uint64_t message_int = stoull(bin_encrypted, nullptr, 2);

    uint64_t gpu_decrypted = decrypt_gpu(key_int, message_int);

    if(plain_int == gpu_decrypted)
        cout << "SUCCESS!" << endl;
}

vector<string> generate_subkeys(std::string key)
{
    string key_plus = "";

    for(auto i = 0; i < 56; i++)
    {
        auto index = permutation1[i];
        key_plus.append(key, index - 1, 1);
    }

    auto c0 = key_plus.substr(0, 28);
    auto d0 = key_plus.substr(28, 28);

    vector<string> k = { key_plus };
    vector<string> c = { c0 };
    vector<string> d = { d0 };

    for(auto n = 1; n <= 16; n++)
    {
        bitset<28> cn(c[n - 1]);
        bitset<28> dn(d[n - 1]);

        rotate(cn, shifts1[n]);
        rotate(dn, shifts1[n]);

        c.push_back(cn.to_string());
        d.push_back(dn.to_string());

        auto kn = cn.to_string();
        kn.append(dn.to_string());
        string kn_permutated = "";

        for(auto j = 0; j < 48; j++)
        {
            auto index = permutation2[j];
            kn_permutated.append(kn, index - 1, 1);
        }
        k.push_back(kn_permutated);
    }

    return k;
}

string des_encrypt_block_cpu(std::string key, std::string message)
{
    auto k = generate_subkeys(key);



    string message_permutated = "";

    for(auto i = 0; i < 64; i++)
    {
        auto index = initial_permutation[i];
        message_permutated.append(message, index - 1, 1);
    }

    auto l0 = message_permutated.substr(0, 32);
    auto r0 = message_permutated.substr(32, 32);

    vector<string> l = { l0 };
    vector<string> r = { r0 };

    for(auto n = 1; n <= 16; n++)
    {
        l.push_back(r[n - 1]);

        string e_rn = "";

        for(auto i = 0; i < 48; i++)
        {
            auto index = e_bit_table[i];
            e_rn.append(r[n - 1], index - 1, 1);
        }

        bitset<48> e_rn_bitset(e_rn);
        bitset<48> nkey_bitset(k[n]);

        auto key_rn_bitset = e_rn_bitset ^ nkey_bitset;
        auto key_rn_string = key_rn_bitset.to_string();

        string s_group_sum = "";

        for(auto i = 0; i < 8; i++)
        {
            auto s_group = key_rn_string.substr(i * 6, 6);

            auto row_string(s_group.substr(0, 1));
            row_string.append(s_group.substr(5, 1));
            auto row = strtol(row_string.c_str(), nullptr, 2);

            auto col_string(s_group.substr(1, 4));
            auto col = strtol(col_string.c_str(), nullptr, 2);

            auto val = s[i][row * 16 + col];
            auto val_string = bitset< 64 >(val).to_string().substr(60, 4);

            s_group_sum.append(val_string);
        }

        string f_result = "";

        for(auto i = 0; i < 32; i++)
        {
            auto index = P[i];
            f_result.append(s_group_sum, index - 1, 1);
        }

        bitset<32> ln_bitset(l[n - 1]);
        bitset<32> f_bitset(f_result);

        auto result_bitset = ln_bitset ^ f_bitset;
        auto result_string = result_bitset.to_string();

        r.push_back(result_string);
    }

    auto final_string = r[16];
    final_string.append(l[16]);

    string final_permutated = "";

    for(auto i = 0; i < 64; i++)
    {
        auto index = final_permutation[i];
        final_permutated.append(final_string, index - 1, 1);
    }

    return BinaryStringToHexString(final_permutated);
}

string des_decrypt_block_cpu(std::string key, std::string encrypted_message)
{
    auto keys = generate_subkeys(key);

    string depermutated(64, 0);

    for(auto i = 0; i < 64; i++)
    {
        auto index = final_permutation[i];
        depermutated[index - 1] = encrypted_message[i];
    }

    auto r16 = depermutated.substr(0, 32);
    auto l16 = depermutated.substr(32, 32);

    vector<string> r(16, "");
    vector<string> l(16, "");

    r.push_back(r16);
    l.push_back(l16);

    for(auto n = 15; n >= 0; n--)
    {
        r[n] = l[n + 1];

        string e_rn = "";

        for(auto i = 0; i < 48; i++)
        {
            auto index = e_bit_table[i];
            e_rn.append(r[n], index - 1, 1);
        }

        bitset<48> e_rn_bitset(e_rn);
        bitset<48> nkey_bitset(keys[n + 1]);

        auto key_rn_bitset = e_rn_bitset ^ nkey_bitset;
        auto key_rn_string = key_rn_bitset.to_string();

        string s_group_sum = "";

        for(auto i = 0; i < 8; i++)
        {
            auto s_group = key_rn_string.substr(i * 6, 6);

            auto row_string(s_group.substr(0, 1));
            row_string.append(s_group.substr(5, 1));
            auto row = strtol(row_string.c_str(), nullptr, 2);

            auto col_string(s_group.substr(1, 4));
            auto col = strtol(col_string.c_str(), nullptr, 2);

            auto val = s[i][row * 16 + col];
            auto val_string = bitset< 64 >(val).to_string().substr(60, 4);

            s_group_sum.append(val_string);
        }

        string f_result = "";

        for(auto i = 0; i < 32; i++)
        {
            auto index = P[i];
            f_result.append(s_group_sum, index - 1, 1);
        }

        bitset<32> ln_bitset(r[n + 1]);
        bitset<32> f_bitset(f_result);

        auto result_bitset = ln_bitset ^ f_bitset;
        auto result_string = result_bitset.to_string();

        l[n] = result_string;
    }

    string initial_permutated = "";
    initial_permutated.append(l[0]);
    initial_permutated.append(r[0]);

    string decrypted_message(64, 0);

    for(auto i = 0; i < 64; i++)
    {
        auto index = initial_permutation[i];
        decrypted_message[index - 1] = initial_permutated[i];
    }

    return BinaryStringToHexString(decrypted_message);
}

uint64_t decrypt_gpu(uint64_t key, int64_t encrypted_message)
{
    bool c[17][28];
    bool d[17][28];
    uint64_t k[17] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    for(int i = 0; i < 28; i++)
    {
        int index = permutation1[i] - 1;
        c[0][i] = (key >> (63 - index)) & 1;

        index = permutation1[i + 28] - 1;
        d[0][i] = (key >> (63 - index)) & 1;
    }

    for(int n = 1; n <= 16; n++)
    {
        int nshifts = shifts1[n];

        for(int i = 0; i < 28; i++)
        {
            int new_index = i - nshifts;
            if(new_index < 0) new_index += 28;

            c[n][new_index] = c[n - 1][i];
            d[n][new_index] = d[n - 1][i];
        }

        bool kn[56];

        for(int i = 0; i < 28; i++)
        {
            kn[i] = c[n][i];
            kn[i + 28] = d[n][i];
        }

        for(int i = 0; i < 48; i++)
        {
            int index = permutation2[i] - 1;
            bool val = kn[index];
            k[n] ^= (-val ^ k[n]) & (static_cast<uint64_t>(1) << (63 - i));
        }
    }

    uint64_t message_depermutated = 0;

    for(int i = 0; i < 64; i++)
    {
        int index = final_permutation[i] - 1;
        bool value = (encrypted_message >> (63 - i)) & static_cast<uint64_t>(1);
        message_depermutated ^= (-value ^ message_depermutated) & (static_cast<uint64_t>(1) << (63 - index));
    }

    uint32_t r[17];
    uint32_t l[17];

    r[16] = (message_depermutated >> 32) & 0xFFFFFFFFull;
    l[16] = message_depermutated & 0xFFFFFFFFull;

    for(int n = 15; n >= 0; n--)
    {
        r[n] = l[n + 1];

        uint64_t e_rn = 0;

        for(int i = 0; i < 48; i++)
        {
            int index = e_bit_table[i] - 1;
            bool val = (r[n] >> (31 - index)) & 1;
            e_rn ^= (-val ^ e_rn) & (static_cast<uint64_t>(1) << (63 - i));
        }

        uint64_t xor_res = e_rn ^ k[n + 1];

        uint32_t s_groups_sum = 0;

        for(int i = 0; i < 8; i++)
        {
            uint8_t row = 0, col = 0;
            bool val = 0;

            val = (xor_res >> (63 - (i * 6))) & 1;
            row ^= (-val ^ row) & (static_cast<uint8_t>(1) << 1);
            val = (xor_res >> (63 - ((i * 6) + 5))) & 1;
            row ^= (-val ^ row) & (static_cast<uint8_t>(1) << 0);

            for(int j = 1; j <= 4; j++)
            {
                val = (xor_res >> (63 - ((i * 6) + j))) & 1;
                col ^= (-val ^ col) & (static_cast<uint8_t>(1) << (4 - j));
            }

            uint8_t part = s[i][row * 16 + col];

            for(int j = 0; j < 4; j++)
            {
                val = (part >> (3 - j)) & 1;
                s_groups_sum ^= (-val ^ s_groups_sum) & (static_cast<uint32_t>(1) << (31 - (i * 4 + j)));
            }
        }

        uint32_t f_res = 0;

        for(int i = 0; i < 32; i++)
        {
            int index = P[i] - 1;
            bool val = (s_groups_sum >> (31 - index)) & 1;
            f_res ^= (-val ^ f_res) & (static_cast<uint32_t>(1) << (31 - i));
        }

        l[n] = r[n + 1] ^ f_res;
    }

    bool initial_permutated[64];
    uint64_t decrypted_message = 0;

    for(int i = 0; i < 32; i++)
    {
        initial_permutated[i] = (l[0] >> (31 - i)) & 1;
        initial_permutated[i + 32] = (r[0] >> (31 - i)) & 1;
    }

    for(int i = 0; i < 64; i++)
    {
        int index = initial_permutation[i] - 1;
        bool value = initial_permutated[i];
        decrypted_message ^= (-value ^ decrypted_message) & (static_cast<uint64_t>(1) << (63 - index));
    }

    return decrypted_message;
}