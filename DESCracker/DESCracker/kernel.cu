#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdint.h>
#include <string>

__constant__ char permutation1[] = {
    57, 49, 41, 33, 25, 17, 9,
    1, 58, 50, 42, 34, 26, 18,
    10, 2, 59, 51, 43, 35, 27,
    19, 11, 3, 60, 52, 44, 36,
    63, 55, 47, 39, 31, 23, 15,
    7, 62, 54, 46, 38, 30, 22,
    14, 6, 61, 53, 45, 37, 29,
    21, 13, 5, 28, 20, 12, 4
};

__constant__ char shifts1[] = {
    0, 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};

__constant__ char permutation2[] = {
    14, 17, 11, 24, 1, 5,
    3, 28, 15, 6, 21, 10,
    23, 19, 12, 4, 26, 8,
    16, 7, 27, 20, 13, 2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32
};

__constant__ char initial_permutation[] = {
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17, 9, 1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7
};

__constant__ char e_bit_table[] = {
    32, 1, 2, 3, 4, 5,
    4, 5, 6, 7, 8, 9,
    8, 9, 10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32, 1
};

__constant__ char s1[] = {
    14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
    0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
    4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
    15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
};

__constant__ char s2[] = {
    15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
    3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
    0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
    13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
};

__constant__ char s3[] = {
    10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
    13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
    13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
    1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
};

__constant__ char s4[] = {
    7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
    13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
    10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
    3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
};

__constant__ char s5[] = {
    2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
    14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
    4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
    11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
};

__constant__ char s6[] = {
    12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
    10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
    9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
    4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
};

__constant__ char s7[] = {
    4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
    13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
    1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
    6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
};

__constant__ char s8[] = {
    13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
    1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
    7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
    2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
};

__constant__ char *s[] = { s1, s2, s3, s4, s5, s6, s7, s8 };

__constant__ char P[] = {
    16, 7, 20, 21,
    29, 12, 28, 17,
    1, 15, 23, 26,
    5, 18, 31, 10,
    2, 8, 24, 14,
    32, 27, 3, 9,
    19, 13, 30, 6,
    22, 11, 4, 25
};

__constant__ char final_permutation[] = {
    40, 8, 48, 16, 56, 24, 64, 32,
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41, 9, 49, 17, 57, 25
};

__device__ void GenerateSubkeys(uint64_t key, uint64_t* keys)
{
    bool c[17][28];
    bool d[17][28];

    for(int i = 0; i < 28; i++)
    {
        int index = permutation1[i] - 1;
        c[0][i] = (key >> (63 - index)) & 1;

        index = permutation1[i + 28] - 1;
        d[0][i] = (key >> (63 - index)) & 1;
    }

    for(int n = 1; n <= 16; n++)
    {
        int nshifts = shifts1[n];

        for(int i = 0; i < 28; i++)
        {
            int new_index = i - nshifts;
            if(new_index < 0) new_index += 28;

            c[n][new_index] = c[n - 1][i];
            d[n][new_index] = d[n - 1][i];
        }

        bool kn[56];

        for(int i = 0; i < 28; i++)
        {
            kn[i] = c[n][i];
            kn[i + 28] = d[n][i];
        }

        keys[n] = 0;
        for(int i = 0; i < 48; i++)
        {
            int index = permutation2[i] - 1;
            bool val = kn[index];
            keys[n] ^= (-val ^ keys[n]) & (static_cast<uint64_t>(1) << (63 - i));
        }
    }
}

__device__ void ExtractInitialHalfs(uint64_t *subkeys, uint32_t *r, uint32_t *l)
{
    for(int n = 15; n >= 0; n--)
    {
        r[n] = l[n + 1];

        uint64_t e_rn = 0;

        for(int i = 0; i < 48; i++)
        {
            int index = e_bit_table[i] - 1;
            bool val = (r[n] >> (31 - index)) & 1;
            e_rn ^= (-val ^ e_rn) & (static_cast<uint64_t>(1) << (63 - i));
        }

        uint64_t xor_res = e_rn ^ subkeys[n + 1];

        uint32_t s_groups_sum = 0;

        for(int i = 0; i < 8; i++)
        {
            uint8_t row = 0, col = 0;
            bool val = 0;

            val = (xor_res >> (63 - (i * 6))) & 1;
            row ^= (-val ^ row) & (static_cast<uint8_t>(1) << 1);
            val = (xor_res >> (63 - ((i * 6) + 5))) & 1;
            row ^= (-val ^ row) & (static_cast<uint8_t>(1) << 0);

            for(int j = 1; j <= 4; j++)
            {
                val = (xor_res >> (63 - ((i * 6) + j))) & 1;
                col ^= (-val ^ col) & (static_cast<uint8_t>(1) << (4 - j));
            }

            uint8_t part = s[i][row * 16 + col];

            for(int j = 0; j < 4; j++)
            {
                val = (part >> (3 - j)) & 1;
                s_groups_sum ^= (-val ^ s_groups_sum) & (static_cast<uint32_t>(1) << (31 - (i * 4 + j)));
            }
        }

        uint32_t f_res = 0;

        for(int i = 0; i < 32; i++)
        {
            int index = P[i] - 1;
            bool val = (s_groups_sum >> (31 - index)) & 1;
            f_res ^= (-val ^ f_res) & (static_cast<uint32_t>(1) << (31 - i));
        }

        l[n] = r[n + 1] ^ f_res;
    }
}

__global__ void DecryptMessage(uint64_t* keys, int n, bool* is_correct, int64_t encrypted_message, int64_t plaintext_message)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i >= n) return;

    uint64_t subkeys[17];
    GenerateSubkeys(keys[i], subkeys);

    uint64_t message_depermutated = 0;

    for(int i = 0; i < 64; i++)
    {
        int index = final_permutation[i] - 1;
        bool value = (encrypted_message >> (63 - i)) & static_cast<uint64_t>(1);
        message_depermutated ^= (-value ^ message_depermutated) & (static_cast<uint64_t>(1) << (63 - index));
    }

    uint32_t r[17];
    uint32_t l[17];

    r[16] = (message_depermutated >> 32) & 0xFFFFFFFFull;
    l[16] = message_depermutated & 0xFFFFFFFFull;

    ExtractInitialHalfs(subkeys, r, l);

    bool initial_permutated[64];
    uint64_t decrypted_message = 0;

    for(int i = 0; i < 32; i++)
    {
        initial_permutated[i] = (l[0] >> (31 - i)) & 1;
        initial_permutated[i + 32] = (r[0] >> (31 - i)) & 1;
    }

    for(int i = 0; i < 64; i++)
    {
        int index = initial_permutation[i] - 1;
        bool value = initial_permutated[i];
        decrypted_message ^= (-value ^ decrypted_message) & (static_cast<uint64_t>(1) << (63 - index));
    }

    is_correct[i] = (decrypted_message == plaintext_message);
}

void CallDecryptionKernel(uint64_t* d_keys, int n, bool* d_is_correct, int64_t d_encrypted_message, int64_t d_plaintext_message)
{
    int block_size = 32;
    dim3 threads(block_size);
    dim3 blocks(n / (float)block_size + 1);
    DecryptMessage <<< blocks, threads >>>(d_keys, n, d_is_correct, d_encrypted_message, d_plaintext_message);
}
