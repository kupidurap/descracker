#include "cuda_runtime.h"

#include <cstdio>

#include "cpu_des.hpp"
#include <atomic>
#include "utils.hpp"
#include <iostream>

using namespace std;

cudaError_t DesBruteAttack();
void CallDecryptionKernel(uint64_t* d_keys, int n, bool* d_is_correct, 
                          int64_t d_encrypted_message, int64_t d_plaintext_message);

int main()
{
    auto cuda_status = DesBruteAttack();
    if (cuda_status != cudaSuccess) {
        fprintf(stderr, "AddWithCuda failed!");
        return 1;
    }

    cuda_status = cudaDeviceReset();
    if (cuda_status != cudaSuccess) {
        fprintf(stderr, "cudaDeviceReset failed!");
        return 1;
    }

    return 0;
}

cudaError_t DesBruteAttack()
{
    auto cuda_status = cudaSuccess;
    
    uint64_t *d_keys = nullptr;
    bool *d_is_correct = nullptr;
    const auto size = 10000;

    string hex_key = "133457799BBCDFF1";
    auto bin_key = HexStringToBinaryString(hex_key);

    string hex_message = "0123456789ABCDEF";
    auto bin_message = HexStringToBinaryString(hex_message);
    cout << "plaintext message: " << hex_message << endl;

    auto encrypted_message = des_encrypt_block_cpu(bin_key, bin_message);
    cout << "encrypted message: " << encrypted_message << endl;

    auto bin_encrypted = HexStringToBinaryString(encrypted_message);

    uint64_t key_int = stoull(bin_key, nullptr, 2);
    uint64_t message_int = stoull(bin_encrypted, nullptr, 2);
    uint64_t plain_int = stoull(bin_message, nullptr, 2);
     
    uint64_t keys[size];
    bool is_correct[size];
    for(auto i = 0; i < (size - 1); i++)
    {
        keys[i] = i;
        is_correct[i] = false;
    }
    is_correct[size - 1] = false;
    keys[size - 1] = key_int;

    cuda_status = cudaSetDevice(0);
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    cuda_status = cudaMalloc(reinterpret_cast<void**>(&d_keys), size * sizeof(uint64_t));
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cuda_status = cudaMalloc(reinterpret_cast<void**>(&d_is_correct), size * sizeof(bool));
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cuda_status = cudaMemcpy(d_keys, keys, size * sizeof(uint64_t), cudaMemcpyHostToDevice);
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cuda_status = cudaMemcpy(d_is_correct, is_correct, size * sizeof(bool), cudaMemcpyHostToDevice);
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    CallDecryptionKernel(d_keys, size, d_is_correct, message_int, plain_int);

    // Check for any errors launching the kernel
    cuda_status = cudaGetLastError();
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cuda_status));
        goto Error;
    }

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cuda_status = cudaDeviceSynchronize();
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cuda_status);
        goto Error;
    }

    cuda_status = cudaMemcpy(is_correct, d_is_correct, size * sizeof(bool), cudaMemcpyDeviceToHost);
    if(cuda_status != cudaSuccess)
    {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    for(auto i = 0; i < size; i++)
    {
        if(is_correct[i])
            cout << "SUCCESS: " << keys[i] << endl;
    }

Error:
    cudaFree(d_is_correct);
    cudaFree(d_keys);

    return cuda_status;
}
