#include <algorithm>
#include <stdexcept>
#include <string>
#include <iostream>
#include <map>
#include <fstream>
#include <inttypes.h>
using namespace std;

map<string, string> bin_to_hex_map = {
    { "0000", "0" },
    { "0001", "1" },
    { "0010", "2" },
    { "0011", "3" },
    { "0100", "4" },
    { "0101", "5" },
    { "0110", "6" },
    { "0111", "7" },
    { "1000", "8" },
    { "1001", "9" },
    { "1010", "A" },
    { "1011", "B" },
    { "1100", "C" },
    { "1101", "D" },
    { "1110", "E" },
    { "1111", "F" }
};

string uint64tobinstr(uint64_t val)
{
    string res = "";
    int i = 0;

    do
    {
        res.append((val & 1) ? "1" : "0");
        val >>= 1;
        i++;
    } while(i < 64);

    std::reverse(std::begin(res), std::end(res));
    return res;
}

string uint32tobinstr(uint64_t val)
{
    string res = "";
    int i = 0;

    do
    {
        res.append((val & 1) ? "1" : "0");
        val >>= 1;
        i++;
    } while(i < 32);

    std::reverse(std::begin(res), std::end(res));
    return res;
}

string uint8tobinstr(uint64_t val)
{
    string res = "";
    int i = 0;

    do
    {
        res.append((val & 1) ? "1" : "0");
        val >>= 1;
        i++;
    } while(i < 8);

    std::reverse(std::begin(res), std::end(res));
    return res;
}

string DecimalToBinaryString(int a)
{
    string binary = "";
    int mask = 0x80000000u;
    while(mask > 0)
    {
        binary += ((a & mask) == 0) ? '0' : '1';
        mask >>= 1;
    }
    cout << binary << endl;
    return binary;
}

string hex_char_to_bin(char c)
{
    switch(toupper(c))
    {
        case '0': return "0000";
        case '1': return "0001";
        case '2': return "0010";
        case '3': return "0011";
        case '4': return "0100";
        case '5': return "0101";
        case '6': return "0110";
        case '7': return "0111";
        case '8': return "1000";
        case '9': return "1001";
        case 'A': return "1010";
        case 'B': return "1011";
        case 'C': return "1100";
        case 'D': return "1101";
        case 'E': return "1110";
        case 'F': return "1111";
    }

    return "";
}

string HexStringToBinaryString(const string& input)
{
    string result = "";

    for(auto& c : input)
    {
        result.append(hex_char_to_bin(c));
    }

    return result;
}

string bin_group_to_hex_char(const string& group)
{
    return bin_to_hex_map[group];
}

string BinaryStringToHexString(const string& input)
{
    string result = "";

    for(auto i = 0; i < input.length(); i+=4)
    {
        auto byte_group = input.substr(i, 4);
        result.append(bin_group_to_hex_char(byte_group));
    }
    return result;
}
