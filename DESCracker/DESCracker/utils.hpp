#include <string>
#include <bitset>
#include <inttypes.h>

template <std::size_t N>
inline void
rotate(std::bitset<N>& b, unsigned m)
{
    b = b << m | b >> (N - m);
}

std::string DecimalToBinaryString(int a);
std::string HexCharToBin(char c);

std::string HexStringToBinaryString(const std::string& input);
std::string BinaryStringToHexString(const std::string& input);
std::string uint64tobinstr(uint64_t val);
std::string uint32tobinstr(uint64_t val);
std::string uint8tobinstr(uint64_t val);